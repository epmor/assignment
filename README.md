# Assignment one

# Private Repository

NPTC Business Solution is a local software house specialising in Java OOP powered applications.  They have been approached by a small private learning provider “eTeach”, who are seeking advice on how best to update their current enrolment administration systems.  

The development team are impressed with your UML diagrams and would like you to build an application derived from your UML class diagrams.   Whenever possible you are to develop code that implements design patterns. Where design patterns have been employed a detailed evaluation of the design pattern used, its purpose and its benefits.
