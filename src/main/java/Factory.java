/*
 * (C) Copyright 2018 Ethan Paul Morgan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
*/

import java.util.Scanner;


public class Factory {
    AdminStaff adminStaff;
    Student student;
    Tutor tutor;
    //Program program;
    Enrolment enrolment;
    
    public void ConstructEnrolment() {

	this.adminStaff = new AdminStaff();
	this.student = new Student();
        this.tutor = new Tutor();
        //this.program = new Program(1,"Title", 2, "Leader", "StartDate", "EndDate", 1, "Module1", 2, "Module2", 3, "Module3", 4, "Module4", 5, "Module5", 6, "Module6");

        this.enrolment = new Enrolment();

        Scanner MenuInput = new Scanner(System.in); 
        String Option ="";

        do {
            for (int i = 0; i < 100; i++) { System.out.println();  }
            System.out.println("Main Menu"); 
            System.out.println("============================="); 
            System.out.println("S   Student Menu" );
            System.out.println("T   Tutor Menu");
            System.out.println("A   Staff Menu");
            System.out.println("P   Program Menu");
            System.out.println("E   Enrolment Menu");
            System.out.println("X   Exit");

            for (int i = 0; i < 5; i++) { System.out.println();  }
            System.out.print(" Select Option (S,T,A, P, E, X) ");
            Option = MenuInput.next();

            if (Option.equals("S") ) { student.DetailsMenu();     } 
            if (Option.equals("T") ) { tutor.DetailsMenu();         } 
            if (Option.equals("A") ) { adminStaff.DetailsMenu();  } 
	    // if (Option.equals("P") ) { program.DetailsMenu();    } 
            if (Option.equals("E") ) { enrolment.DetailsMenu(); } 
        } while (!Option.equals("X"));
    }
}
