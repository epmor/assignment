/*
 * (C) Copyright 2018 Ethan Paul Morgan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Student implements DetailsMenu  {
    public String FirstName;
    public int    StudentID;
    public String MiddleName;
    public String LastName;
    public String DOB;
    public String Street;
    public String Area;
    public String Town;
    public String PostCode;
    public String HomeNumber;
    public String MobileNumber;
    public int    ProgramID;
    public String Program;
    public String Email;
    public String StartDate;
    public double Score;
    public String Grade;

    @Override
    public void DetailsMenu() {
        Scanner MenuInput = new Scanner(System.in); 
        String Option ="";
        
        do {
            for (int i = 0; i < 100; i++) { System.out.println();  }
            System.out.println("Student Menu"); 
            System.out.println("============================="); 
            System.out.println("I   Input Student Details" );
            System.out.println("V   View Student Details");
            System.out.println("S   Save Student Details");
            System.out.println("X   Exit");
            System.out.println("");
            System.out.println("");

            for (int i = 0; i < 5; i++) { System.out.println();  }
            System.out.print(" Select Option (I,V,S) ");
            Option = MenuInput.next();

            if (Option.equals("V") ) { OutputDetails();  } 
            if (Option.equals("I") ) { InputDetails();  } 
            if (Option.equals("I") ) { SaveDetails();  } 
            if (Option.equals("A") ) { AutoInputDetails();  } 
        } while (!Option.equals("X"));        	
    }
    
    @Override
    public void InputDetails() {
	Scanner userInput = new Scanner(System.in); 
        boolean valid = false;
	do {
            try{
                valid = true;
                System.out.print("Enter Student ID -    ");
                StudentID = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
		valid = false;
            }
	    if( !valid ) System.out.println("Invalid Enter Student ID- ");           
        }while( !valid );
                
        System.out.print("Enter First Name - ");
        FirstName = userInput.next();
        
        System.out.print("Enter Middle Names - ");
        MiddleName = userInput.next();
        
        System.out.print("Enter Last Name - ");
        LastName = userInput.next();
        
        valid = false;
        do {
            try{
                System.out.print("Enter DOB - ");
                DOB = userInput.next();
                if(Integer.parseInt(DOB.substring(0, 2))>0  && Integer.parseInt(DOB.substring(0, 2)) <= 31){
                    if(Integer.parseInt(DOB.substring(3, 5))>0  && Integer.parseInt(DOB.substring(3, 5))<= 12){
                        if(Integer.parseInt(DOB.substring(6, 10)) > 1900  && Integer.parseInt(DOB.substring(6, 10))<= 2000){
			    valid = true;
                        }
                    }
                }
            }
            catch (Exception e){
                
            }
	    if( !valid ) System.out.println("Invalid DOB - ");           
        }while( !valid );
        
        System.out.print("Enter Street and house No - ");
        FirstName = userInput.next();
        
        System.out.print("Enter Area - ");
        LastName = userInput.next();
        
        System.out.print("Enter Town - ");
        Street = userInput.next();
        
        System.out.print("Enter Postcode - ");
        PostCode = userInput.next();
        
        System.out.print("Enter HomeNumber - ");
        HomeNumber = userInput.next();
        
        System.out.print("Enter Mobile Number - ");
        MobileNumber = userInput.next();
        
        System.out.print("Enter Email - ");
        Email = userInput.next();
        
        do {
            try{
                valid = true;
                System.out.print("Enter Program ID -    ");
                ProgramID = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
		valid = false;
            }
	    if( !valid ) System.out.println("Invalid Must ID - ");           
        }while( !valid );
        
        
        System.out.print("Enter Program - ");
        Program = userInput.next();
        
        System.out.print("Enter Start Date - ");
        StartDate = userInput.next();
        
        
        
	do {
            try{
                valid = true;
                System.out.print("Enter Program Score -    ");
                Score = Double.parseDouble(userInput.next());
            }
            catch (Exception e){
		valid = false;
            }
	    if( !valid ) System.out.println("Invalid Must ID - ");           
        }while( !valid );
              
        System.out.print("Enter Grade - ");
        Grade = userInput.next();  
        
        System.out.print(" Press P then enter to cont."); 
        Scanner MenuInput = new Scanner(System.in); 
        MenuInput.next();
    }

    @Override
    public void OutputDetails() {
	System.out.println(" First Name -    " + FirstName);
        System.out.println(" Middle Names -  " + MiddleName);
        System.out.println(" Last Name -     " + LastName);
        System.out.println(" DOB -           " + DOB);
        System.out.println(" Area -          " + Area);
        System.out.println(" Town -          " + Street);
        System.out.println(" Postcode -      " + PostCode);
        System.out.println(" HomeNumber -    " + HomeNumber);
        System.out.println(" Mobile Number - " + MobileNumber);
        System.out.println(" Email -         " + Email);
        System.out.println(" Program ID -    " + ProgramID);
        System.out.println(" Enter Program - " + Program);
        System.out.println(" Start Date -    " + StartDate);
        System.out.println(" Program Score - " + Score);
        System.out.println(" Grade -         " + Grade);  
        
        System.out.print(" Press P then enter to cont."); 
        Scanner MenuInput = new Scanner(System.in); 
        MenuInput.next();     
    }
    
    @Override
    public void AutoInputDetails() {
	this.FirstName    = "Ethan"  ;
        this.MiddleName   = "Paul";
        this.LastName     = "Morgan";
        this.DOB          = "12/06/1998";
        this.Street       = "Brahams Avanue";
        this.Area         = "Port Talbot";
        this.Town         = "Aberavon";
        this.PostCode     = "SA12 7TY";
        this.HomeNumber   = "01639 513453";
        this.MobileNumber = "07450778990";
        this.Email        = "Ethan@epmor.net";
        this.ProgramID    = 3;
        this.Program      = "Hnd Computing";
        this.StartDate    = "12/09/2018";
        this.Score        = 98.63;
        this.Grade        = "M";
    }
    
    @Override
    public void ViewDetails() {
	
    }
    
    @Override
    public void SaveDetails() {
	PrintWriter WriterStudentFile = null;
        try {
            //throws Exception {
            File StudentFile = new File("AdminFile.Dat");
            Boolean Append = true;
            WriterStudentFile = new PrintWriter(new FileOutputStream( StudentFile, Append));
            WriterStudentFile.println(FirstName);
            WriterStudentFile.println(MiddleName);
            WriterStudentFile.println(LastName);
            WriterStudentFile.println(Area);
            WriterStudentFile.println(Street);
            WriterStudentFile.println(PostCode);
            WriterStudentFile.println(HomeNumber);
            WriterStudentFile.println(MobileNumber);
            WriterStudentFile.println(Email);
            WriterStudentFile.println(ProgramID);
            WriterStudentFile.println(Program);
            WriterStudentFile.println(StartDate);
            WriterStudentFile.println(Score);
            WriterStudentFile.println(Grade);
            WriterStudentFile.close();
            System.out.println("Record Saved.");  
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Student.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            WriterStudentFile.close();
        }
        
        System.out.print(" Press P then enter to cont."); 
        Scanner MenuInput = new Scanner(System.in); 
        MenuInput.next();	
    }
}
