/*
 * (C) Copyright 2018 Ethan Paul Morgan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Logger;


public class Program implements DetailsMenu {
    public int    ProgramID;
    public String Title;
    public String FirstName;
    public int    StudentID;
    public String MiddleName;
    public int    Level;
    public String Leader;
    public String StartDate;
    public String EndDate;

    public int    ModuleID1;
    public String Module1;

    public int    ModuleID2;
    public String Module2;

    public int    ModuleID3;
    public String Module3;

    public int    ModuleID4;
    public String Module4;

    public int    ModuleID5;
    public String Module5;

    public int    ModuleID6;
    public String Module6;


    public Program(int    ProgramID,
		   String Title,
		   String FirstName,
		   int    StudentID,
		   String MiddleName,
		   int    Level,
		   String Leader,
		   String StartDate,
		   String EndDate,
		   int    ModuleID1,
		   String Module1,
		   int    MmoduleID2,
		   String Module2,
		   int    ModuleID3,
		   String Module3,
		   int    ModuleID4,
		   String Module4,
		   int    ModuleID5,
		   String Module5,
		   int    ModuleID6,
		   String Module6) {
	
	this.ProgramID = ProgramID;
        this.Title     = Title;
        this.Level     = Level;
        this.Leader    = Leader;
        this.StartDate = StartDate;
        this.EndDate   = EndDate;
        this.ModuleID1 = ModuleID1;
        this.Module1   = Module1;
        this.ModuleID2 = ModuleID2;
        this.Module2   = Module2;
        this.ModuleID3 = ModuleID3;
        this.Module3   = Module3;
        this.ModuleID4 = ModuleID4;
        this.Module4   = Module4;
        this.ModuleID5 = ModuleID5;
        this.Module5   = Module5;
        this.ModuleID6 = ModuleID6;
        this.Module6   = Module6;
    }
    
    @Override
    public void DetailsMenu() {
        Scanner MenuInput = new Scanner(System.in); 
        String Option ="";
        
        do {
            for (int i = 0; i < 100; i++) { System.out.println();  }
            System.out.println("Program Menu"); 
            System.out.println("============================="); 
            System.out.println("I   Input Program Details" );
            System.out.println("V   View Program Details");
            System.out.println("S   Save Program Details");
            System.out.println("X   Exit");
            System.out.println("");
            System.out.println("");

            for (int i = 0; i < 5; i++) { System.out.println();  }
            System.out.print(" Select Option (I,V,S) ");
            Option = MenuInput.next();

            if (Option.equals("V") ) { OutputDetails();  } 
            if (Option.equals("I") ) { InputDetails();  } 
            if (Option.equals("I") ) { SaveDetails();  } 
            if (Option.equals("A") ) { AutoInputDetails();  } 
        } while (!Option.equals("X"));        
     }       	
    
    @Override
    public void InputDetails() {
	Scanner userInput = new Scanner(System.in); 
        boolean valid = false;
        
        do {
            try{
                valid = true;
                System.out.print("Enter Program ID -    ");
                ProgramID = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Must ID - ");           
        }while( !valid );
             
        System.out.print("Enter Program Title - ");
        Title = userInput.next();
        
         do {
            try{
                valid = true;
                System.out.print("Enter Program Level - ");
                Level = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Must Level - ");           
        }while( !valid );
        
        System.out.print("Enter Program Leader - ");
        Leader = userInput.next();
        
        valid = false;
        do {
            try{
                System.out.print("Enter Start Date - ");
                StartDate = userInput.next();
                if(Integer.parseInt(StartDate.substring(0, 2))>0  && Integer.parseInt(StartDate.substring(0, 2)) <= 31){
                    if(Integer.parseInt(StartDate.substring(3, 5))>0  && Integer.parseInt(StartDate.substring(3, 5))<= 12){
                        if(Integer.parseInt(StartDate.substring(6, 10))>1900  && Integer.parseInt(StartDate.substring(6, 10))<= 2018){
                             valid = true;
                        }
                    }
                }
            }
            catch (Exception e){
                
            }
           if( !valid ) System.out.println("Invalid Date - ");           
        }while( !valid );
        
        valid = false;
        do {
            try{
                System.out.print("Enter End Date - ");
                EndDate = userInput.next();
                if(Integer.parseInt(EndDate.substring(0, 2))>0  && Integer.parseInt(EndDate.substring(0, 2)) <= 31){
                    if(Integer.parseInt(EndDate.substring(3, 5))>0  && Integer.parseInt(EndDate.substring(3, 5))<= 12){
                        if(Integer.parseInt(EndDate.substring(6, 10))>1900  && Integer.parseInt(EndDate.substring(6, 10))<= 2018){
                             valid = true;
                        }
                    }
                }
            }
            catch (Exception e){
                
            }
           if( !valid ) System.out.println("Invalid Date - ");           
        }while( !valid );
        
         do {
            try{
                valid = true;
                System.out.print("Enter Module Id - ");
                ModuleID1 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid );
        
        System.out.print("Enter Module Title - ");
        Module1 = userInput.next();
        
        do {
            try{
                valid = true;
                System.out.print("Enter Module Id - ");
                ModuleID2 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid );
        
              
        System.out.print("Enter Module Title - ");
        Module2 = userInput.next();
        
       do {
            try{
                valid = true;
                System.out.print("Enter Module Id - ");
                ModuleID3 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid );
       
        System.out.print("Enter Module Title - ");
        Module3 = userInput.next();
        
        do {
            try{
                valid = true;
                System.out.print("Enter Module Id - ");
                ModuleID4 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid );
        
        System.out.print("Enter Module Title - ");
        Module4 = userInput.next();
        
        do {
            try{
                valid = true;
                System.out.print("Enter Module Id - ");
                ModuleID5 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid );
        
       
        System.out.print("Enter Module Title - ");
        Module5 = userInput.next();
        
        do {
            try{
                valid = true;
                System.out.print("Enter Module Id - ");
                ModuleID6 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid );
        
    }

    @Override
    public void OutputDetails() {
	System.out.println("  ProgramID -    " + ProgramID);
        System.out.println("  Title -        " + Title);
        System.out.println("  Level -        " + Level );
        System.out.println("  Leader -       " + Leader );
        System.out.println("  StartDate -    " + StartDate );
        System.out.println("  EndDate -      " + EndDate );
        System.out.println("  ModuleID1 -    " + ModuleID1 );
        System.out.println("  Module1 -      " + Module1 );
        System.out.println("  ModuleID2 -    " + ModuleID2 );
        System.out.println("  Module2 -      " + Module2 );
        System.out.println("  ModuleID3 -    " + ModuleID3 );
        System.out.println("  Module3 -      " + Module3 );
        System.out.println("  ModuleID4 -    " + ModuleID4);
        System.out.println("  Module4 -      " + Module4 );
        System.out.println("  ModuleID5 -    " + ModuleID5 );
        System.out.println("  Module5 -      " + Module5);
        System.out.println("  ModuleID6 -    " + ModuleID6 );
        System.out.println("  Module6 -      " + Module6 );
    }
    
    @Override
    public void AutoInputDetails() {
	this.ProgramID   = 1;
        this.Title       = "Hnd Computing";
        this.Level       = 5;
        this.Leader      = "J Addams";
        this.StartDate   = "01/09/2018";
        this.EndDate     = "31/08/2019";
        this.ModuleID1   = 100;
        this.Module1     = "Programming";
        this.ModuleID2   = 200;
        this.Module2     = "Database";
        this.ModuleID3   = 300;
        this.Module3     = "Spreadsheets";
        this.ModuleID4   = 400;
        this.Module4     = "Compiting Systems";
        this.ModuleID5   = 0;
        this.Module5     = "N/A";
        this.ModuleID6   = 0;
        this.Module6     = "N/A";
    }
    
    @Override
    public void ViewDetails() {}
    
    @Override
    public void SaveDetails() {
        PrintWriter WriterProgramFile = null;
        try {
            File ProgramFile = new File("ProgramFile.Dat");
            Boolean Append = true;
            WriterProgramFile = new PrintWriter(new FileOutputStream( ProgramFile, Append));
            WriterProgramFile.println(ProgramID);
            WriterProgramFile.println(Title);
            WriterProgramFile.println(Level );
            WriterProgramFile.println(Leader );
            WriterProgramFile.println(StartDate );
            WriterProgramFile.println(EndDate );
            WriterProgramFile.println(ModuleID1 );
            WriterProgramFile.println(Module1 );
            WriterProgramFile.println(ModuleID2 );
            WriterProgramFile.println(Module2 );
            WriterProgramFile.println(ModuleID3 );
            WriterProgramFile.println(Module3 );
            WriterProgramFile.println(ModuleID4);
            WriterProgramFile.println(Module4 );
            WriterProgramFile.println(ModuleID5 );
            WriterProgramFile.println(Module5);
            WriterProgramFile.println(ModuleID6 );
            WriterProgramFile.println(Module6 );
            WriterProgramFile.close();
            System.out.println("Record Saved.");  
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Program.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } finally {
            WriterProgramFile.close();
        }	
    }
}
