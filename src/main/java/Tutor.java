/*
 * (C) Copyright 2018 Ethan Paul Morgan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Tutor implements DetailsMenu {
    public String FirstName;
    public int    StudentID;
    public String MiddleName;
    public String LastName;
    public String DOB;
    public String Street;
    public String Area;
    public String Town;
    public String PostCode;
    public String HomeNumber;
    public String MobileNumber;
    public String WorkNumber;
    public String Email;
    public String School;
    public String Subject;

    @Override
    public void DetailsMenu() {
  Scanner MenuInput = new Scanner(System.in); 
        String Option ="";
        
        do {
            for (int i = 0; i < 100; i++) { System.out.println();  }
            System.out.println("Tutor Menu"); 
            System.out.println("============================="); 
            System.out.println("I   Input Tutor Details" );
            System.out.println("V   View Tutor Details");
            System.out.println("S   Save Tutor Details");
            System.out.println("X   Exit");
            System.out.println("");
            System.out.println("");
            
            for (int i = 0; i < 5; i++) { System.out.println();  }
            System.out.print(" Select Option (I,V,S) ");
            Option = MenuInput.next();

            if (Option.equals("V") ) { OutputDetails();  } 
            if (Option.equals("I") ) { InputDetails();  } 
            if (Option.equals("I") ) { SaveDetails();  } 
            if (Option.equals("A") ) { AutoInputDetails();  } 
        } while (!Option.equals("X"));        	
    }
    
    @Override
    public void InputDetails() {
	        Scanner userInput = new Scanner(System.in); 
        
        System.out.print("Enter First Name - ");
        FirstName = userInput.next();
        
        System.out.print("Enter Middle Names - ");
        MiddleName = userInput.next();
        
        System.out.print("Enter Last Name - ");
        FirstName = userInput.next();
        
        
        boolean valid = false;
        do {
            try{
                System.out.print("Enter DOB - ");
                DOB = userInput.next();
                if(Integer.parseInt(DOB.substring(0, 2))>0  && Integer.parseInt(DOB.substring(0, 2)) <= 31){
                    if(Integer.parseInt(DOB.substring(3, 5))>0  && Integer.parseInt(DOB.substring(3, 5))<= 12){
                        if(Integer.parseInt(DOB.substring(6, 10))>1900  && Integer.parseInt(DOB.substring(6, 10))<= 2018){
                             valid = true;
                        }
                    }
                }
            }
            catch (Exception e){
                
            }
           if( !valid ) System.out.println("Invalid DOB - ");           
        }while( !valid );
        
        System.out.print("Enter Street and house No - ");
        FirstName = userInput.next();
        
        System.out.print("Enter Area - ");
        LastName = userInput.next();
        
        System.out.print("Enter Town - ");
        Street = userInput.next();
        
        System.out.print("Enter Postcode - ");
        PostCode = userInput.next();
        
        System.out.print("Enter HomeNumber - ");
        HomeNumber = userInput.next();
        
        System.out.print("Enter Mobile Number - ");
        MobileNumber = userInput.next();
        
        System.out.print("Enter Email - ");
        Email = userInput.next();
        
        System.out.print("Enter Work Number - ");
        WorkNumber = userInput.next();
        
        System.out.print("Enter School - ");
        School = userInput.next();
        
        System.out.print("Enter Subject - ");
        Subject = userInput.next();
    }

    @Override
    public void AutoInputDetails() {
	this.FirstName = "Bill"  ;
        this.MiddleName = "Tom";
        this.LastName = "Smith";
        this.Street = "Crown Road";
        this.Area = "Sanfields";
        this.Town = "Port Talbot";
        this.PostCode = "SA12 8EX";
        this.HomeNumber = "01639 345765";
        this.MobileNumber = "075 843212";
        this.Email = "Bill@HND.com";
        this.WorkNumber = "01639 648073";
        this.School = "CIT";
        this.Subject = "BSc Computing";   
    }
	
    @Override
    public void OutputDetails() {

        System.out.println(" First Name -    " + FirstName);
        System.out.println(" Middle Names -  " + MiddleName);
        System.out.println(" Last Name -     " + FirstName);
        System.out.println(" Street -        " + FirstName);
        System.out.println(" Area -          " + LastName);
        System.out.println(" Town -          " + Street);
        System.out.println(" Postcode -      " + PostCode);
        System.out.println(" HomeNumber -    " + HomeNumber);
        System.out.println(" Mobile Number - " + MobileNumber);
        System.out.println(" Email -         " + Email);
        System.out.println(" WorkNumber -    " + WorkNumber);
        System.out.println(" School -        " + School);
        System.out.println(" Subject -       " + Subject);	
    }

    @Override
    public void ViewDetails() {}

    
    @Override
    public void SaveDetails() {
	PrintWriter WriterTutorFile = null;
        try {
            File TutorFile = new File("TutorFile.Dat");
            Boolean Append = true;
            WriterTutorFile = new PrintWriter(new FileOutputStream( TutorFile, Append));
            WriterTutorFile.println(FirstName);
            WriterTutorFile.println(MiddleName);
            WriterTutorFile.println(FirstName);
            WriterTutorFile.println(FirstName);
            WriterTutorFile.println(LastName);
            WriterTutorFile.println(Street);
            WriterTutorFile.println(PostCode);
            WriterTutorFile.println(HomeNumber);
            WriterTutorFile.println(MobileNumber);
            WriterTutorFile.println(Email);
            WriterTutorFile.println(WorkNumber);
            WriterTutorFile.println(School);
            WriterTutorFile.println(Subject);
            WriterTutorFile.close();
            System.out.println("Record Saved.");  
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Tutor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            WriterTutorFile.close();
        }
    }
}
