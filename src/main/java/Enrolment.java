/*
 * (C) Copyright 2018 Ethan Paul Morgan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Logger;

public class Enrolment implements DetailsMenu {
    public int    StudentID;
    public int    ProgramID;
    
    public int    ModuleID1;
    public int    ModuleID2;
    public int    ModuleID3;
    public int    ModuleID4;
    public int    ModuleID5;
    public int    ModuleID6;
    
    public int    ModuleScore1;
    public int    ModuleScore2;
    public int    ModuleScore3;
    public int    ModuleScore4;
    public int    ModuleScore5;
    public int    ModuleScore6;
    
    public String ProgramGrade;

    @Override
    public void DetailsMenu() {
    Scanner MenuInput = new Scanner(System.in); 
        String Option ="";
        
        do {
            for (int i = 0; i < 100; i++) { System.out.println();  }
            System.out.println("Enrolment Menu"); 
            System.out.println("============================="); 
            System.out.println("I   Input Enrolmet Details" );
            System.out.println("V   View Enrolmet Details");
            System.out.println("S   Save Enrolmet Details");
            System.out.println("X   Exit");
            System.out.println("");
            System.out.println("");
            
            for (int i = 0; i < 5; i++) { System.out.println();  }
            System.out.print(" Select Option (I,V,S) ");
            Option = MenuInput.next();

            if (Option.equals("V") ) { ViewDetails();  } 
            if (Option.equals("I") ) { InputDetails();  } 
            if (Option.equals("I") ) { SaveDetails();  } 
            if (Option.equals("A") ) { AutoInputDetails();  } 
        } while (!Option.equals("X"));        	
    }
    
    @Override
    public void InputDetails() {
        Scanner userInput = new Scanner(System.in); 
        boolean valid = false;
         do {
            try{
                valid = true;
                System.out.print("Enter Student ID -    ");
                StudentID = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Enter Student ID- ");           
        }while( !valid );
        
         valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Program ID -    ");
                ProgramID = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Must ID - ");           
        }while( !valid );
        
        valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 1 Id - ");
                ModuleID1 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid ); 
        
        valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 1 Score - ");
                ModuleScore1 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Score - ");           
        }while( !valid ); 
       
         valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 2 Id - ");
                ModuleID2 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid ); 
        
        valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 2 Score - ");
                ModuleScore2 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Score - ");           
        }while( !valid ); 
        
         valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 3 Id - ");
                ModuleID3 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid ); 
        
        valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 3 Score - ");
                ModuleScore3 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Score - ");           
        }while( !valid ); 
        
         valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 4 Id - ");
                ModuleID4 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid ); 
        
        valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 4 Score - ");
                ModuleScore4 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Score - ");           
        }while( !valid ); 
        
         valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 5 Id - ");
                ModuleID5 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid ); 
        
        valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 5 Score - ");
                ModuleScore5 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Score - ");           
        }while( !valid ); 
         valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 6 Id - ");
                ModuleID6 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid ID - ");           
        }while( !valid ); 
        
        valid = false;
        do {
            try{
                valid = true;
                System.out.print("Enter Module 6 Score - ");
                ModuleScore6 = Integer.parseInt(userInput.next());
            }
            catch (Exception e){
                 valid = false;
            }
           if( !valid ) System.out.println("Invalid Score - ");           
        }while( !valid ); 
        
        
        System.out.print("Enter program Grade - ");
        ProgramGrade = userInput.next();	
    }

    @Override
    public void AutoInputDetails() {

    }
	
    @Override
    public void OutputDetails() {
        System.out.print("Enter Student ID -    " + StudentID);
        System.out.print("Enter Program ID -    " + ProgramID);
        System.out.print("Enter Module 1 Id -    " + ModuleID1);
        System.out.print("Enter Module 1 Score - "+ ModuleScore1);
        System.out.print("Enter Module 2 Id -    " + ModuleID2);
        System.out.print("Enter Module 2 Score - "+ ModuleScore2);
        System.out.print("Enter Module 3 Id -    " + ModuleID3);
        System.out.print("Enter Module 3 Score - "+ ModuleScore3);
        System.out.print("Enter Module 4 Id -    " + ModuleID4);
        System.out.print("Enter Module 4 Score - "+ ModuleScore4);
        System.out.print("Enter Module 5 Id -    " + ModuleID5);
        System.out.print("Enter Module 5 Score - "+ ModuleScore5);
        System.out.print("Enter Module 6 Id -    " + ModuleID6);
        System.out.print("Enter Module 6 Score - "+ ModuleScore6);           
        System.out.print("Enter program Grade - " + ProgramGrade);	
    }

    @Override
    public void ViewDetails() {}

    
    @Override
    public void SaveDetails() {

        PrintWriter WriterEnrolmentFile = null;
        try {
            File ProgramFile = new File("EnrolmentFile.Dat");
            Boolean Append = true;
            WriterEnrolmentFile = new PrintWriter(new FileOutputStream( ProgramFile, Append));
            WriterEnrolmentFile.println(StudentID);
            WriterEnrolmentFile.println(ProgramID);
            WriterEnrolmentFile.println(ModuleID1 );
            WriterEnrolmentFile.println(ModuleScore1 );
            WriterEnrolmentFile.println(ModuleID2 );
            WriterEnrolmentFile.println(ModuleScore2 );
            WriterEnrolmentFile.println(ModuleID3 );
            WriterEnrolmentFile.println(ModuleScore3 );
            WriterEnrolmentFile.println(ModuleID4);
            WriterEnrolmentFile.println(ModuleScore4 );
            WriterEnrolmentFile.println(ModuleID5 );
            WriterEnrolmentFile.println(ModuleScore5);
            WriterEnrolmentFile.println(ModuleID6 );
            WriterEnrolmentFile.println(ModuleScore6 );
            WriterEnrolmentFile.println(ProgramGrade );
            WriterEnrolmentFile.close();
            System.out.println("Record Saved.");  
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Program.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } finally {
            WriterEnrolmentFile.close();
        }	
    }
}
