/*
 * (C) Copyright 2018 Ethan Paul Morgan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
*/

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AdminStaff implements DetailsMenu {
    public String FirstName;
    public String MiddleName;
    public String LastName;
    public String Street;
    public String Area;
    public String Town;
    public String PostCode;
    public String HomeNumber;
    public String MobileNumber;
    public String Email;


    @Override
    public void DetailsMenu() {
        Scanner MenuInput = new Scanner(System.in); 
        String Option ="";
        
        do {
            for (int i = 0; i < 100; i++) { System.out.println();  }
            System.out.println("Staff Menu"); 
            System.out.println("============================="); 
            System.out.println("I   Input Staff Details" );
            System.out.println("V   View Staff Details");
            System.out.println("S   Save Staff Details");
            System.out.println("X   Exit");
            System.out.println("");
            System.out.println("");
            
            for (int i = 0; i < 5; i++) { System.out.println();  }
            System.out.print(" Select Option (I,V,S) ");
            Option = MenuInput.next();

            if (Option.equals("V") ) { OutputDetails();  } 
            if (Option.equals("I") ) { InputDetails();  } 
            if (Option.equals("I") ) { SaveDetails();  } 
            if (Option.equals("A") ) { AutoInputDetails();  } 
        } while (!Option.equals("X"));        
	
    }
    
    @Override
    public void InputDetails() {
	Scanner userInput = new Scanner(System.in); 
        
        System.out.print("Enter First Name - ");
        FirstName = userInput.next();
        
        System.out.print("Enter Middle Names - ");
        MiddleName = userInput.next();
        
        System.out.print("Enter Last Name - ");
        LastName = userInput.next();
                                    
        System.out.print("Enter Area - ");
        LastName = userInput.next();
        
        System.out.print("Enter Town - ");
        Street = userInput.next();
        
        System.out.print("Enter Postcode - ");
        PostCode = userInput.next();
        
        System.out.print("Enter HomeNumber - ");
        HomeNumber = userInput.next();
        
        System.out.print("Enter Mobile Number - ");
        MobileNumber = userInput.next();
        
        System.out.print("Enter Email - ");
        Email = userInput.next();
    }
    
    @Override
    public void ViewDetails() {
	
    }
    
    @Override
    public void SaveDetails() {
        PrintWriter WriterAdminFile = null;
        try {
            File AdminFile = new File("AdminFile.Dat");
            Boolean Append = true;
            WriterAdminFile = new PrintWriter(new FileOutputStream( AdminFile, Append));
            WriterAdminFile.println(FirstName);
            WriterAdminFile.println(MiddleName);
            WriterAdminFile.println(LastName);
            WriterAdminFile.println(Street);
            WriterAdminFile.println(PostCode);
            WriterAdminFile.println(HomeNumber);
            WriterAdminFile.println(MobileNumber);
            WriterAdminFile.println(Email);
            WriterAdminFile.close();  
            System.out.println("Record Saved.");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AdminStaff.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            WriterAdminFile.close();
        }	
    }
    
    @Override
    public void AutoInputDetails() {
	this.FirstName = "Ethan";
        this.MiddleName = "Paul";
        this.LastName = "Morgan";
        this.Street = "Brahams Avanue";
        this.Area = "Aberavon";
        this.Town = "Port Talbot";
        this.PostCode = "SA12 7TY";
        this.HomeNumber = "01639 698172";
        this.MobileNumber = "07450778990";
        this.Email = "EthanMorgan@epmor.net";
    }
    
    @Override
    public void OutputDetails() {
        System.out.println(" First Name -    " + FirstName);
        System.out.println(" Middle Names -  " + MiddleName);
        System.out.println(" Last Name -     " + LastName);
        System.out.println(" Area -          " + LastName);
        System.out.println(" Town -          " + Street);
        System.out.println(" Postcode -      " + PostCode);
        System.out.println(" HomeNumber -    " + HomeNumber);
        System.out.println(" Mobile Number - " + MobileNumber);
        System.out.println(" Email -         " + Email);	
    }
    
}
